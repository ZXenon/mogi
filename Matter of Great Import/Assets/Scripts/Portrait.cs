﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portrait : MonoBehaviour
{
    public void Clicked()
    {
        PersistentManagerScript.Instance.gameState = "Dialogue";
        PersistentManagerScript.Instance.stateChange();
        PersistentManagerScript.Instance.portraitNum += 1;
        gameObject.SetActive(false);
    }
}
